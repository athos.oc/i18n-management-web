import React from 'react';
import BulletNumber from '../BulletNumber/BulletNumber';
import Card from '../Card/Card';
import './TranslateItem.scss';

const TranslateItem = (props) => {
    console.log('props.value', props.value);

    return (
        <div className="translate-item-container">
            <div className="card-item">
                <Card>
                    <div className="grid-x">
                        <div className="cell medium-offset-1 large-offset-1 medium-11 large-11">{props.value.key}</div>
                    </div>

                    <div className="aaa grid-x">
                        <div className="card-number cell medium-1 large-1">
                            <BulletNumber number={props.index} size={30} />
                        </div>
                        <div className="card-content cell medium-11 large-11">
                            ES <input type="text" placeholder={props.value.value} />
                            EN <input type="text" placeholder={props.value.value} />
                        </div>
                    </div>

                </Card>
            </div>
        </div>
    )

}

export default TranslateItem