import React, {useState} from 'react'
import './Input.scss'
import {
    AiOutlineCloseCircle
} from 'react-icons/ai'

const Input = (props) => {
    const [value1, setValue1] = useState('')
    const [active, setActive] = useState(false)

    const handleCloseClick = () => {
        setValue1('')
        props.onChange('')
    }
    const handleInputChange = (e) => {
        setValue1(e.target.value)
    }
    const handleInputKeyUp = (e) => {
        props.onChange(e.target.value)
    }
    const handleInputBlur = () => {
        setActive(false)
        props.onBlur(value1)
    }

    const handleInputFocus = () => {
        setActive(true)
    }

    return (
        <div className={active===true ? "input-container active": "input-container"}>
            <div className="input">
                <input 
                    placeholder={props.placeHolder} 
                    type="text" 
                    value={value1}
                    onChange={handleInputChange}
                    onBlur={handleInputBlur}
                    onFocus={handleInputFocus}
                    onKeyUp={handleInputKeyUp}
                />
            </div>
            <div className="icon" onClick={handleCloseClick}><AiOutlineCloseCircle/></div>
        </div>
    )
}

export default Input