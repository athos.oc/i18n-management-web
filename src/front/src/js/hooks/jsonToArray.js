
export const jsonToArray = (jsonData) =>{
    const array = [];
    for (const key in jsonData) {
        if (jsonData.hasOwnProperty(key)) {
            const element = jsonData[key];
            array.push({key:key, value:element});
        }
    }
    return array;
}